myHeader = {
	'Accept': 'application/json',
	'Content-Type': 'application/json'
}

async function getList(listId = '') {
	let listUrl = "https://api.trello.com/1/lists/" + listId + "?fields=name%2Cclosed%2CidBoard%2Cpos&" + auth;
	let url = 'https://api.trello.com/1/lists/' + listId + '/cards?fields=id,name,badges,labels&' + auth;

	return await Promise.all([
		fetch(url).then(response => {
			return response.json();
		}),
		fetch(listUrl, {}).then(response => {
			return response.json();
		})
	]);
}

async function getChecklist(cardId = '') {
	let url = 'https://api.trello.com/1/cards/' + cardId + '/checklists?' + auth;
	let cardDataUrl = `https://api.trello.com/1/cards/${cardId}?fields=name,desc&${auth}`;

	return await Promise.all([
		fetch(url).then(response => {
			return response.json();
		}),
		fetch(cardDataUrl, {}).then(response => {
			return response.json();
		})
	]);
}
async function archiveCard(cardId = '') {
	let url = `https://api.trello.com/1/cards/${cardId}?closed=true&${auth}`;
	return new Promise((resolve, reject) => {
		fetch(url, {
			method: 'PUT'
		}).then(response => {
			// console.log(response.json());
			resolve(response.json());
		})

	})
}
async function getItem(checklistId = '', idCheckItem = '') {
	return new Promise((resolve, reject) => {

		let url = `https://api.trello.com/1/checklists/${checklistId}/checkItems/${idCheckItem}?${auth}`;
		fetch(url).then(response => {
			// console.log(response.json());
			resolve(response.json());
		})

	})
}

async function addCard(listId = '', cardName = "parag dinesh gupta") {
	let url = "https://api.trello.com/1/cards?name=" + cardName + "&idList=" + listId + "&keepFromSource=all&" + auth;
	return new Promise((resolve, reject) => {
		fetch(url, {
			method: 'POST'
		}).then(response => {
			resolve(response.json());
		})

	})
}

// https://api.trello.com/1/cards/5d1f1ace5b222b6f91689b87/checkItem/5d1f1b049918f51cfd42ca56?state=complete&
async function setItemState(cardId = '', idCheckItem = '', state = 'incomplete') {

	let url = `https://api.trello.com/1/cards/${cardId}/checkItem/${idCheckItem}?state=${state}&${auth}`;
	return new Promise((resolve, reject) => {
		fetch(url, {
			method: 'PUT'
		}).then(response => {
			resolve(response.json());
		})

	})
}

async function updateItem(cardId = '', idCheckItem = '', name = '') {

	let url = `https://api.trello.com/1/cards/${cardId}/checkItem/${idCheckItem}?name=${name}&${auth}`;
	return new Promise((resolve, reject) => {
		fetch(url, {
			method: 'PUT'
		}).then(response => {
			resolve(response.json());
		})

	})
}

async function deleteItem(checklistId = '', idCheckItem = '') {

	let url = `https://api.trello.com/1/checklists/${checklistId}/checkItems/${idCheckItem}?${auth}`;
	return new Promise((resolve, reject) => {
		fetch(url, {
			method: 'DELETE'
		}).then(response => {
			resolve(response.json());
		})

	})
}

async function deleteChecklist(checklistId = '') {

	let url = `https://api.trello.com/1/checklists/${checklistId}?${auth}`;
	return new Promise((resolve, reject) => {
		fetch(url, {
			method: 'DELETE'
		}).then(response => {
			resolve(response.json());
		})
	})
}

async function addItem(name = 'name', checklistId = '') {
	let url = "https://api.trello.com/1/checklists/" + checklistId + "/checkItems?name=" + name + "&pos=bottom&checked=false&" + auth;
	return new Promise((resolve, reject) => {
		fetch(url, {
			method: 'POST'
		}).then(response => {
			resolve(response.json());
		})

	})
}

async function addChecklist(name = 'name', cardId = '') {
	let url = `https://api.trello.com/1/cards/${cardId}/checklists?name=${name}&${auth}`;
	return new Promise((resolve, reject) => {
		fetch(url, {
			method: 'POST'
		}).then(response => {
			resolve(response.json());
		})

	})
}