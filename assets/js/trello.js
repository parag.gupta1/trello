// let BoardId='tBmYPSYe'; //public
// let listId='560bf44ea68b16bd0fc2a9a9'; //public
// let listId = '5cf780fad66abf5a98ba8e25';

let BoardId = 'jk3AqGvO';
let listId = '5d1f1ac082b80a0f7da4a625';
let auth = 'key=a65571083b4757351fa6e33e9d977871&token=1155087855604371708c6ab99f03bba06c693c827ca13326c95fe5969d9925d2';
let listContainer = getElement("#list-container");
let modal = getElement("#myModal");
let checklistList = getElement(".checklist-list");

listContainer.addEventListener('click', actionOnClick, true);
modal.addEventListener('click', actionOnModalClick, true);

window.addEventListener("load", (event) => {
	showLists();
})


function showLists() {
	listContainer.innerHTML = "";
	getList(listId)
		.then(results => {
			listShowOnScreen(results[0], results[1]);
		})
}

function listShowOnScreen(list, listData) {

	let cardContainer = createElement('div');
	listContainer.appendChild(cardContainer);
	cardContainer.className = "card-container";
	cardContainer.id = listData["id"];
	let listName = createElement('span');
	listName.innerText = listData["name"];
	listName.className = "list-name";
	cardContainer.appendChild(listName);

	list.forEach((card) => {
		let myCard = createElement("div");
		myCard.className = "card";
		cardContainer.appendChild(myCard);
		myCard.id = card["id"];
		let myCardText = createElement("span");
		myCardText.className = "card-text";
		myCardText.innerText = card["name"];
		myCard.appendChild(myCardText);
		let cardArchive = createElement("a");
		cardArchive.href = "#";
		cardArchive.title = "Archive";
		cardArchive.className = "fa fa-archive";
		// cardArchive.innerText = "x";
		myCard.appendChild(cardArchive);
	})
	let listFooter = createElement("div");
	listFooter.className = "list-footer";
	cardContainer.appendChild(listFooter);
	let addNewCard = createElement("a");
	addNewCard.href = "#";
	addNewCard.innerText = "Add new card";
	listFooter.appendChild(addNewCard);

	let listFooter2 = createElement("div");
	listFooter2.className = "list-footer2";
	listFooter2.style.display = "none";
	cardContainer.appendChild(listFooter2);
	let newCardInput = createElement("input");
	let newCardSubmit = createElement("button");
	newCardSubmit.innerText = "Add";
	newCardSubmit.id = listData["id"];
	newCardSubmit.className = "new-card-submit";
	newCardInput.className = "new-card-input";
	listFooter2.appendChild(newCardInput);
	listFooter2.appendChild(newCardSubmit);
}

function getChecklistId(ev) {
	return new Promise((resolve, reject) => {
		let checkListId;
		for (var i = 0; i < ev.path.length; i++) {

			if (ev.path[i]["className"] == "checklist") {
				checkListId = ev.path[i]["id"];
				break;
			}
		}
		resolve(checkListId);
	})
}

async function actionOnModalClick(ev) {

	if (ev.target.type == "checkbox") {

		let item = ev.target.parentElement;
		let itemId = item["id"];
		let cardId = checklistList["id"];
		if (ev.target.checked) {
			var response = await setItemState(cardId, itemId, "complete");
		} else {
			var response = await setItemState(cardId, itemId, "incomplete");
		}
		let checklistId = await getChecklistId(ev);
		let responseItem = await getItem(checklistId, itemId);
		insertItem(responseItem, item);

	} else if (ev.target.className == "add-an-item") {
		openAddItem(ev.target);
	} else if (ev.target.className == "cancel-checklist-item") {
		closeAddItem(ev.target);
	} else if (ev.target["className"] == "add-item-button") {
		let itemAddList = getElements(".add-item-button");
		let itemInputList = getElements(".add-item-input");
		let index = [].indexOf.call(itemAddList, ev.target);
		let itemName = itemInputList[index]["value"];
		let checklistId = await getChecklistId(ev);
		let response = await addItem(itemName, checklistId);
		// let cardId = await getCardId(ev);
		prepareModel();
	} else if (ev.target["id"] == "add-checklist-button") {
		let checklistAddButton = getElement("#add-checklist-button");
		let checklistInput = getElement("#add-checklist-input");
		let checklistName = checklistInput["value"];
		let cardId = getElement(".checklist-list")["id"];
		let response = await addChecklist(checklistName, cardId);
		checklistInput["value"] = "";
		prepareModel();
	} else if (ev.target["className"] == "delete-item") {
		let deleteItemButton = getElements(".delete-item");
		let item = getElements(".item");
		let index = [].indexOf.call(deleteItemButton, ev.target);
		let itemId = item[index]["id"];
		let checklistId = await getChecklistId(ev);
		let response = await deleteItem(checklistId, itemId);
		let cardId = await getCardId(ev);
		prepareModel();
	} else if (ev.target["className"] == "checklist-delete") {
		let deleteChecklistButton = getElements(".checklist-delete");
		let checklist = getElements(".checklist");
		let index = [].indexOf.call(deleteChecklistButton, ev.target);
		let checklistId = checklist[index]["id"];
		// let checklistId = await getChecklistId(ev);
		let response = await deleteChecklist(checklistId);
		prepareModel();
	} else if (ev.target["className"] == "item-text") {
		ev.target.contentEditable = "true";
		let itemText = getElements(".item-text");
		let updateItem = getElements(".update-item");
		let index = [].indexOf.call(itemText, ev.target);
		updateItem[index].style.display = "block";
		ev.target.style.padding = "10px 5px";
	} else if (ev.target["className"] == "update-item") {
		let item = ev.target.parentElement;
		let itemId = item["id"];
		let itemText;
		let itemChildren = item.children;
		for (var i = 0; i < itemChildren.length; i++) {
			if (itemChildren[i].className == "item-text") {
				itemText = itemChildren[i].innerText;
				break;
			}
		};
		let cardId = await getCardId(ev);
		let response = await updateItem(cardId, itemId, itemText)
		let checklistId = await getChecklistId(ev);
		let responseItem = await getItem(checklistId, itemId);
		insertItem(responseItem, item);
	}

}

function openAddItem(element) {

	let elementList = getElements(".add-an-item");
	let elementList2 = getElements(".add-item-form");
	let index = [].indexOf.call(elementList, element);

	elementList2[index].style.display = "block";
	elementList[index].style.display = "none";
}

function closeAddItem(element) {

	let elementList = getElements(".cancel-checklist-item");
	let elementList2 = getElements(".add-item-form");
	let elementList3 = getElements(".add-an-item");
	let index = [].indexOf.call(elementList, element);

	elementList2[index].style.display = "none";
	elementList3[index].style.display = "inline";
}

async function actionOnClick(ev) {

	console.log(ev.target.className);
	for (var i = 0; i < ev.path.length; i++) {
		if (ev.path[i]["className"] == "fa fa-archive") {
			let response = await archiveCard(ev.target.parentElement["id"]);
			showLists();
			break;
		} else if (ev.path[i]["className"] == "card") {
			prepareModel(ev.path[i]["id"]);
			break;
		} else if (ev.path[i]["className"] == "list-footer") {
			openAddCardForm(ev.path[i]);
			break;
		} else if (ev.target["className"] == "new-card-submit") {
			let cardAddList = getElements(".new-card-submit");
			let cardInputList = getElements(".new-card-input");
			let index = [].indexOf.call(cardAddList, ev.target);
			let listId = cardAddList[index]["id"];
			let cardTitle = cardInputList[index].value;
			let response = await addCard(listId, cardTitle);
			showLists();
			break;
		}

	};

}

function openAddCardForm(element) {
	let listFooter = document.querySelectorAll(".list-footer");
	let index = [].indexOf.call(listFooter, element);
	let listFooter2 = document.querySelectorAll(".list-footer2");

	listFooter2[index].style.display = "block";
	listFooter[index].style.display = "none";
}

function clearModal() {
	checklistList.innerHTML = "";
}


function checklistsShowOnModal(myChecklist) {
	myChecklist.forEach((checklist) => {
		makeModal(checklist);
	})
}

function insertItem(item, itemLi) {

	itemLi.innerHTML = "";
	let checkbox = createElement("input");
	checkbox.type = "checkbox";

	let itemText = createElement("p");
	itemText.className = "item-text";
	itemText.innerText = item["name"];

	if (item["state"] == "complete") {
		itemText.style.textDecoration = "line-through";
		checkbox.checked = "true";
	}

	let deleteButton = createElement("button");
	deleteButton.className = "delete-item";
	deleteButton.innerText = "delete";
	let updateButton = createElement("button");
	updateButton.className = "update-item";
	updateButton.innerText = "update";

	itemLi.appendChild(checkbox);
	itemLi.appendChild(itemText);
	itemLi.appendChild(deleteButton);
	itemLi.appendChild(updateButton);
}


function makeModal(myChecklist) {

	let checklist = createElement("div");
	let checklistTitle = createElement("div");
	let checklistProgress = createElement("div");
	let checklistItemsList = createElement("div");
	let itemList = createElement("ol");
	itemList.className = "item-list";
	let addNewItem = createElement("div");
	let addAnItem = createElement("button");
	let addItemForm = createElement("div");
	let addItemInput = createElement("textarea");
	let addItemButton = createElement("button");
	let cancelChecklistItem = createElement("button");

	checklist.appendChild(checklistTitle);
	checklist.appendChild(checklistProgress);
	checklist.appendChild(checklistItemsList);
	checklistItemsList.appendChild(itemList);

	checklist.className = "checklist";
	checklist.id = myChecklist["id"];
	checklistTitle.className = "checklist-title";
	checklistTitle.innerText = myChecklist["name"];
	let checklistDelete = createElement("button");
	checklistDelete.innerText = "Delete";
	checklistDelete.className = "checklist-delete";
	checklistTitle.appendChild(checklistDelete);
	checklistProgress.className = "checklist-progress";
	checklistItemsList.className = "checklist-items-list";

	addNewItem.className = "add-new-item";
	addAnItem.className = "add-an-item";
	addAnItem.innerText = "Add an item"
	addItemForm.className = "add-item-form";
	addItemInput.className = "add-item-input";
	addItemInput.placeholder = "Add an item";
	addItemButton.className = "add-item-button";
	addItemButton.innerText = "Add";
	cancelChecklistItem.className = "cancel-checklist-item";
	cancelChecklistItem.innerText = "cancel"

	checklistItemsList.appendChild(itemList);

	myChecklist["checkItems"].forEach((item) => {

		let itemLi = createElement("li");
		itemLi.className = "item";
		itemLi.id = item["id"];

		let checkbox = createElement("input");
		checkbox.type = "checkbox";

		let itemText = createElement("p");
		itemText.className = "item-text";
		itemText.innerText = item["name"];

		if (item["state"] == "complete") {
			itemText.style.textDecoration = "line-through";
			checkbox.checked = "true";
		}

		let deleteButton = createElement("button");
		deleteButton.className = "delete-item";
		deleteButton.innerText = "delete";
		let updateButton = createElement("button");
		updateButton.className = "update-item";
		updateButton.innerText = "update";

		itemLi.appendChild(checkbox);
		itemLi.appendChild(itemText);
		itemLi.appendChild(deleteButton);
		itemLi.appendChild(updateButton);

		itemList.appendChild(itemLi);
	});

	checklistItemsList.appendChild(addNewItem);
	addNewItem.appendChild(addAnItem);
	addNewItem.appendChild(addItemForm);
	addItemForm.appendChild(addItemInput);
	addItemForm.appendChild(addItemButton);
	addItemForm.appendChild(cancelChecklistItem);
	checklistList.appendChild(checklist);
}

function showModal(cardId = "") {
	modal.style.display = "block";
}

let modalCloser = getElement(".close");

modalCloser.addEventListener("click", () => {
	modal.style.display = "none";
})

window.addEventListener("click", (event) => {

	if (event.target == modal) {
		modal.style.display = "none";
	}
})


function getElement(id) {
	return (document.querySelector(id));
}

function getElements(id) {
	return (document.querySelectorAll(id));
}

function createElement(tag) {
	return document.createElement(tag);
}

function print(data) {
	console.log(data);
}

function getCardId(ev) {
	return new Promise((resolve, reject) => {
		let cardId;
		for (var i = 0; i < ev.path.length; i++) {

			if (ev.path[i]["className"] == "checklist-list") {
				cardId = ev.path[i]["id"];
				break;
			}
		}
		resolve(cardId);
	})
}
async function prepareModel(cardId = checklistList["id"]) {
	clearModal();
	let checklists = await getChecklist(cardId);
	console.log(checklists);
	checklistsShowOnModal(checklists[0], checklists[1]);
	checklistList["id"] = cardId;
	getElement("#card-name").innerText = checklists[1]["name"];
	getElement("#card-desc").innerText = checklists[1]["desc"] || "There is no description";
	showModal(cardId);
}

// document.addEventListener('keydown', function (event) {
//   if (event.keyCode === 13 && event.target.nodeName ===  input_types = "input, select, button, textarea";) {
//     var form = event.target.form;
//     var index = [].indexOf.call(form, event.target);
//     form.elements[index + 1].focus();
//     event.preventDefault();
//   }
// });